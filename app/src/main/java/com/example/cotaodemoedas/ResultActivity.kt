package com.example.cotaodemoedas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class ResultActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        val intent = intent
        if (intent != null) {
            val value = intent.getStringExtra("value")
            val fromText = intent.getStringExtra("fromText")

            var baseValue = 0.0;

            if( fromText == "Real" ) {
                baseValue = 1.0;
            } else if( fromText == "Dólar" ) {
                baseValue = 5.29;
            } else if( fromText == "Euro" ) {
                baseValue = 5.71;
            } else if( fromText == "Peso" ) {
                baseValue = 0.081;
            }

            println("RESULTADO" + value)

            if( value != null ) {
                val textReal = findViewById<TextView>(R.id.textReal)
                textReal.text = (value.toDouble() * (baseValue/1.0) ).toString();

                val textDolar = findViewById<TextView>(R.id.textDolar)
                textDolar.text = (value.toDouble() * (baseValue/5.29) ).toString();

                val textEuro = findViewById<TextView>(R.id.textEuro)
                textEuro.text = (value.toDouble() * (baseValue/5.71) ).toString();

                val textPeso = findViewById<TextView>(R.id.textPeso)
                textPeso.text = (value.toDouble() * (baseValue/0.081) ).toString();
            }


        }

    }
}
