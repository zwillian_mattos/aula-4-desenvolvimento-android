package com.example.cotaodemoedas

import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val from: Spinner = findViewById(R.id.from)
        val to: Spinner = findViewById(R.id.to)
        val toVal: EditText = findViewById(R.id.toVal)

        toVal.setFocusable(false)
        toVal.setEnabled(false)

        ArrayAdapter.createFromResource(
            this,
            R.array.select_moedas,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            from.adapter = adapter
            to.adapter = adapter
        }

        calculate.setOnClickListener{
            val x = fromVal.text.toString().toDouble();
            toVal.setText(convert(x).toString())
        }

        calculaTodos.setOnClickListener{
            var resultado = Intent(this@MainActivity, ResultActivity::class.java)
            resultado.putExtra("value", fromVal.text.toString())
            resultado.putExtra("fromText", from.selectedItem.toString())
            startActivity(resultado)
        }
    }

    fun convert(value: Double): Double {

        var baseValue: Double = 0.0;

        val fromText: String = from.selectedItem.toString()
        val toText: String = to.selectedItem.toString()

        if( fromText == "Real" ) {
            baseValue = 1.0;
        } else if( fromText == "Dólar" ) {
            baseValue = 5.29;
        } else if( fromText == "Euro" ) {
            baseValue = 5.71;
        } else if( fromText == "Peso" ) {
            baseValue = 0.081;
        }

        var result = 0.0

        if( toText == "Real" ) {
            result = value * (baseValue/1.0)
        } else if( toText == "Dólar" ) {
            result = value * (baseValue/5.29);
        } else if( toText == "Euro" ) {
            result = value * (baseValue/5.71);
        } else if( toText == "Peso" ) {
            result = value * (baseValue/0.081);
        }

        /// TODO MELHORAR LóGICA !!!

        println(result)
        return result;
    }
}
